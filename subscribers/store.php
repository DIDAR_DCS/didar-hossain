<?php
// collect data
$email = $_POST['email'];
$is_subscribed = $_POST['is_subscribed'];
$created_at = $_POST['created_at'];
$modified_at = $_POST['modified_at'];
$reason = $_POST['reason'];

$is_subscribed = 0;
if(array_key_exists('is_subscribed',$_POST)){
    $is_subscribed = $_POST['is_subscribed'];
}

//connect to database
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "phpcrud";

//connecting to database
$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
// prepare insert query
//selection query


$query = "INSERT INTO `subscribers` (

`email`,
`is_subscribed`,
`created_at`,
`modified_at`,
`reason`)
 VALUES (

:email,
:is_subscribed, 
:created_at,
:modified_at,
:reason
 );";

//echo $query;
$sth = $conn->prepare($query);
$sth->bindParam(':email',$email);
$sth->bindParam(':is_subscribed',$is_subscribed);
$sth->bindParam(':created_at',$created_at);
$sth->bindParam(':modified_at',$modified_at);
$sth->bindParam(':reason',$reason);
$result = $sth->execute();


header("location:create.php");
?>

