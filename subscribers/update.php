<?php
include_once($_SERVER["DOCUMENT_ROOT"]."/phpcrud/bootstrap.php");
//collect the data
$id = $_POST['id'];
$email = $_POST['email'];
$is_subscribed = $_POST['is_subscribed'];
$created_at = $_POST['created_at'];
$modified_at = $_POST['modified_at'];
$reason = $_POST['reason'];



$query = "UPDATE `subscribers` SET 
`email` = :email ,
`is_subscribed` = :is_subscribed ,
`created_at` = :created_at ,
`modified_at` = :modified_at, 
`reason` = :reason 

WHERE `subscribers`.`id` = :id";

//echo $query;die();

$sth = $conn->prepare($query);
$sth->bindParam(':id',$id);
$sth->bindParam(':email',$email);
$sth->bindParam(':is_subscribed',$is_subscribed);
$sth->bindParam(':created_at',date('Y-m-d h:i:s',time()));
$sth->bindParam(':modified_at',date('Y-m-d h:i:s',time()));
$sth->bindParam(':reason',$reason);
$result = $sth->execute();

//redirect to index page
header("location:index.php");
