<?php
include_once($_SERVER["DOCUMENT_ROOT"]."/phpcrud/bootstrap.php");
//selection query
$query = "SELECT * FROM subscribers ORDER BY id DESC";
$sth = $conn->prepare($query);
$sth->execute();
$subscribers = $sth->fetchAll(PDO::FETCH_ASSOC);
?>

<?php
ob_start();
?>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
        <h1 >Subscribers</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <button type="button" class="btn btn-sm btn-outline-secondary">
                <span data-feather="calendar"></span>
<!--                <a href="--><?//=VIEW;?><!--product/active.php" style="color: black">Active Products</a>-->
<!--                | <a href="--><?//=VIEW;?><!--product/inactive.php" style="color: black">In active Products</a>-->
                | <a href="<?=VIEW;?>subscribers/create.php" style="color: black">Add New</a>
            </button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 ftco-animate">
            <div class="cart-list">
                <table class="table">
                    <thead class="thead-primary">
                    <tr class="text-center">
                        <th>&nbsp</th>
                        <th>Email</th>
                        <th>Is Subscribed</th>
                        <th>Created At</th>
                        <th>Modified_At</th>
                        <th>Reason</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <?php
                    if($subscribers){
                        foreach($subscribers as $subscriber){
                            ?>
                            <tr class="text-center">
                                <td class="subscribers_people">&nbsp;</td>

                                <td class="email-id">
                                    <h3><a href="#?id=<?php echo $subscriber['id'] ?>"><?php echo $subscriber['email'];?></a></h3>
                                </td>
                                <td class="is-subscribed">
                                    <p>
                                        <?php echo $subscriber['is_subscribed']; ?>
                                    </p>
                                </td>
                                <td class="created_at">
                                    <p>
                                        <?php echo $subscriber['created_at']; ?>
                                    </p>
                                </td>
                                <td class="modified_at">
                                    <p>
                                        <?php echo $subscriber['modified_at']; ?>
                                    </p>
                                </td>
                                <td class="reason">
                                    <p>
                                        <?php echo $subscriber['reason']; ?>
                                    </p>
                                </td>


                                <td> <a href="<?=VIEW?>subscribers/edit.php?id=<?php echo $subscriber['id']?>">Edit</a>
                                    | <a href="<?=VIEW?>subscribers/delete.php?id=<?php echo $subscriber['id']?>">Delete</a></td>
                            </tr>
                        <?php }}else{
                        ?>
                        <tr class="text-center">
                            <td colspan="5">
                                There is no product available. <a href="create.php">Click Here</a> to add a subscribers.
                            </td>
                        </tr>
                        <?php
                    }
                    ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>



</main>
<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace("##MAIN_CONTENT##",$pagecontent,$layout);
?>


