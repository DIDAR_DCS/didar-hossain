<?php
include_once($_SERVER["DOCUMENT_ROOT"]."/phpcrud/bootstrap.php");
//selection query
$id = $_GET['id'];
$query = 'SELECT * FROM subscribers WHERE id = :id';
$sth = $conn->prepare($query);
$sth->bindParam(':id',$id);
$sth->execute();

$subscriber = $sth->fetch(PDO::FETCH_ASSOC);

?>

<?php
ob_start();
?>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <form id="subscribers-form" method="post"
          action="update.php"
          enctype="multipart/form-data"
          role="form">
        <div class="messages"></div>
        <h3>Edit Subscriber</h3>
        <div class="controls">
            <div class="row">
                <input id="id"  value="<?php echo $subscriber['id'] ?>" type="hidden" name="id" class="form-control">
            </div>


                            <div class="col-lg-8">
                            <div class="form-group">
                                <label for="email">E-Mail</label>
                                <input
                                        id="email"
                                        value="<?php echo $subscriber['email']?>"
                                        type="text"
                                        name="email"
                                        placeholder="Subscribers Email"
                                        autofocus="autofocus"
                                        class="form-control">
                                <div class="help-block text-muted">Subscribers Email</div>
                                <div class="help-block with-errors"></div>
                            </div>
                            </div>

<!--                            <div class="col-lg-8">-->
<!--                            <div class="form-group">-->
<!--                                <label for="is_subscribed">Is Subscribed</label>-->
<!--                                <input type="number"-->
<!--                                       class="form-control"-->
<!--                                       id="is_subscribed"-->
<!--                                       name="is_subscribed"-->
<!--                                       value="--><?php //echo $subscriber['is_subscribed']?><!--"-->
<!--                                       aria-describedby="is_subscribed"-->
<!--                                       placeholder="">-->
<!--                            </div>-->
<!--                            </div>-->

            <?php
            $checked = '';
            if($subscriber['is_subscribed']){
                $checked = 'checked="checked"';
            }
            ?>

            <div class="col-lg-6">
                <div class="form-group">
                    <label for="is_subscribed">Active</label>
                    <input id="is_subscribed" <?=$checked?> value="1" type="checkbox" name="is_active" class="form-control">
                    <div class="help-block with-errors"></div>
                </div>
            </div>

                            <div class="col-lg-8">
                <div class="form-group">
                    <label for="created_at">Created AT</label>
                    <input type="datetime"
                           class="form-control"
                           id="created_at"
                           name="created_at"
                           value="<?php echo $subscriber['created_at']?>"
                           aria-describedby="created_at"
                           placeholder="">
                </div>
            </div>

            <div class="col-lg-8">
                <div class="form-group">
                    <label for="modified_at">Modified At</label>
                    <input type="datetime"
                           class="form-control"
                           id="modified_at"
                           name="modified_at"
                           value="<?php echo $subscriber['modified_at']?>"
                           aria-describedby="modified_at"
                           placeholder="">
                </div>
            </div>

                            <div class="col-lg-8">
                            <div class="form-group">
                                <label for="reason">Reason</label>
                                <input type="text"
                                       class="form-control"
                                       id="reason"
                                       name="reason"
                                       value="<?php echo $subscriber['reason']?>"
                                       aria-describedby="modified_at"
                                       placeholder="">
                            </div>
                        </div>


            <button type="submit" class="btn btn-success">
                Send & Save Subscribers
            </button>


        </div>

    </form>
</main>
<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace("##MAIN_CONTENT##",$pagecontent,$layout);
?>



