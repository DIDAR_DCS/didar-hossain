<?php
include_once($_SERVER["DOCUMENT_ROOT"]."/phpcrud/bootstrap.php");
//collect the data
$id = $_POST['id'];
$title = $_POST['title'];
$is_active = 0;
if(array_key_exists('is_active',$_POST)){
    $is_active = $_POST['is_active'];
}
$is_uploaded = false;
if($_FILES['picture']['size'] > 0){
    $target_file = $_FILES['picture']['tmp_name'];
    $filename = time()."_".str_replace(' ','-',$_FILES['picture']['name']);
    $dest_file = $_SERVER['DOCUMENT_ROOT'].'/phpcrud/uploads/'.$filename;
    $is_uploaded = move_uploaded_file($target_file, $dest_file);
}
if($is_uploaded){
    $dest_filename = $filename;
}else{
    $dest_filename = $prev_picture_name;
}

$query = "UPDATE `pages` SET 
`title` = :title 

WHERE `pages`.`id` = :id";

$sth = $conn->prepare($query);
$sth->bindParam(':id',$id);
$sth->bindParam(':title',$title);
$result = $sth->execute();

//redirect to index page
header("location:index.php");
