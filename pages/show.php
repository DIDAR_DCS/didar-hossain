<?php
include_once($_SERVER["DOCUMENT_ROOT"] . "/phpcrud/bootstrap.php");
//selection query
$id = $_GET['id'];
$query = 'SELECT * FROM pages WHERE id = :id';
$sth = $conn->prepare($query);
$sth->bindParam(':id', $id);
$sth->execute();
$product = $sth->fetch(PDO::FETCH_ASSOC);
?>

<?php
ob_start();
?>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">


    <form id="product-entry-form" method="post" action="store.php" role="form">

        <div class="messages"></div>
        <h1>ADD NEW</h1>
        <div class="controls">
            <div class="row">
                <div class="col-lg-6">
                    &nbsp;
                </div>

                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="title">Enter Page Title</label>
                        <input id="title"
                               value=""
                               type="text"
                               name="title"
                               placeholder="e.g. Home Page"
                               autofocus="autofocus"
                               class="form-control">
                        <div class="help-block text-muted">Enter Page Title</div>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="description">Description</label>
                        <input id="description" value="" type="text"
                               name="description"
                               class="form-control">
                        <div class="help-block with-errors"></div>

                    </div>
                </div>


                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="link">Link</label>
                        <input id="link" value="" type="text" name="link"
                               class="form-control">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>


                <button type="submit" class="btn btn-success">
                    Send & Save pages
                </button>


            </div>

    </form>
</main>
<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace("##MAIN_CONTENT##", $pagecontent, $layout);
?>
