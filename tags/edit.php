<?php
include_once($_SERVER["DOCUMENT_ROOT"]."/phpcrud/bootstrap.php");
//selection query
$id = $_GET['id'];
$query = 'SELECT * FROM tags WHERE id = :id';
$sth = $conn->prepare($query);
$sth->bindParam(':id',$id);
$sth->execute();

$tag = $sth->fetch(PDO::FETCH_ASSOC);

?>

<?php
ob_start();
?>
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">



        <form id="product-entry-form" method="post"
              action="update.php"
              enctype="multipart/form-data"
              role="form">
            <div class="messages"></div>
            <h1>Edit Product</h1>
            <div class="controls">
                <div class="row">
                    <input id="id"  value="<?php echo $tag['id']?>" type="hidden" name="id" class="form-control">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="id">id</label>
                            <input id="id"  value="" type="number" name="id" class="form-control">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="title">Enter Tags Name</label>
                            <input id="title"
                                   value="<?php echo $tag['title']?>"
                                   type="text"
                                   name="title"
                                   placeholder="e.g.Tags Name"
                                   autofocus="autofocus"
                                   class="form-control">
                            <div class="help-block text-muted">Enter Tags Name</div>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-success">
                        Send & Save Product
                    </button>


                </div>

        </form>
    </main>
<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace("##MAIN_CONTENT##",$pagecontent,$layout);
?>
