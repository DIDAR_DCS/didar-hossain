
<?php
include_once($_SERVER["DOCUMENT_ROOT"] . "/phpcrud/bootstrap.php");
ob_start();
?>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <form id="subscribers-form"
          method="post"
          action="store.php"
          enctype="multipart/form-data"
          role="form">
        <div class="messages"></div>
        <h1>ADD NEW</h1>
        <section>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">

                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="id">ID</label>
                                    <input type="number"
                                           class="form-control"
                                           id="id"
                                           name="id"
                                           value=""
                                           aria-describedby="id"
                                           placeholder="">
                                </div>

                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input type="text"
                                           class="form-control"
                                           id="title"
                                           name="title"
                                           value=""
                                           aria-describedby="title"
                                           placeholder=""
                                           autofocus="autofocus">
                                </div>


                            </div>

                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-success">
                    Send & Save Product
                </button>
    </form>
    </div>
    </section>
</main>
<?php
$pagecontent = ob_get_contents();
ob_end_clean();
echo str_replace("##MAIN_CONTENT##", $pagecontent, $layout);
?>

